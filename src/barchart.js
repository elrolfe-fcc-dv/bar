let chartData;

const displayData = () => {
  const width = 1000;
  const height = 450;
  const xMargin = 60;
  const yMarginTop = 20;
  const yMarginBottom = 30;
  const barWidth = width / chartData.data.length + 0.05;
  const svg = d3
    .select("svg")
    .attr("width", width + xMargin * 2)
    .attr("height", height + yMarginTop + yMarginBottom);

  d3
    .select("#title")
    .text(chartData.name);

  d3
    .select("#description")
    .html(chartData.description.replace(/\n/g, "<br>"));

  const xScale = d3
    .scaleTime()
    .domain([
      d3.min(chartData.data, d => new Date(d[0])),
      d3.max(chartData.data, d => new Date(d[0]))
    ])
    .range([xMargin, width + xMargin]);

  const yScale = d3
    .scaleLinear()
    .domain([
      0,
      d3.max(chartData.data, d => d[1])
    ])
    .range([height + yMarginTop, yMarginTop]);

  const xAxis = d3.axisBottom(xScale);
  const yAxis = d3.axisLeft(yScale);

  svg
    .selectAll("rect")
    .data(chartData.data)
    .enter()
    .append("rect")
    .attr("x", d => xScale(new Date(d[0])))
    .attr("y", d => yScale(d[1]))
    .attr("width", d => barWidth)
    .attr("height", d => height + yMarginTop - yScale(d[1]))
    .attr("data-date", d => d[0])
    .attr("data-gdp", d => d[1])
    .attr("class", "bar")
    .on("mouseover", (d, i) => {
      const dataDate = new Date(d[0]);
      const dateDisplay = dataDate.toLocaleString("en-US", { month: "long", year: "numeric" });
      
      d3.select("#tooltip > p")
        .text(dateDisplay);

      d3.select("#tooltip > h2")
        .text(`$${d[1]} Billion`);

      d3.select("#tooltip")
        .style("opacity", 1)
        .style("left", (d3.event.pageX + 30) + "px")
        .style("top", "450px")
        .attr("data-date", d[0]);
    })
    .on("mouseout", () => {
      d3.select("#tooltip")
        .style("opacity", 0);
    });

  svg
    .append("g")
    .attr("id", "x-axis")
    .attr("transform", "translate(0, " + (height + yMarginTop) + ")")
    .call(xAxis);

  svg
    .append("g")
    .attr("id", "y-axis")
    .attr("transform", "translate(" + xMargin + ", 0)")
    .call(yAxis);
};

const loadData = () => {
  const req = new XMLHttpRequest();
  req.open("GET", "https://raw.githubusercontent.com/freeCodeCamp/ProjectReferenceData/master/GDP-data.json", true);
  req.onload = () => {
    chartData = JSON.parse(req.responseText);
    displayData();
  };
  req.send();
};

document.addEventListener("DOMContentLoaded", loadData);
